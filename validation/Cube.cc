// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this fKpath except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/////////////////////////////////////////////////////////////////////
// compare to https://doi.org/10.1038/s41598-019-46089-9
/////////////////////////////////////////////////////////////////////

#include "../utilities/assign_lattice_to.h"
#include "../utilities/circular_beam_properties.h"
#include "../utilities/compute_eigenvalues.h"
#include "../utilities/read_config.h"
#include "ae108/assembly//plugins/AssembleMassMatrixPlugin.h"
#include "ae108/assembly/Assembler.h"
#include "ae108/cpppetsc/Context.h"
#include "ae108/cpppetsc/GeneralizedMeshBoundaryCondition.h"
#include "ae108/cpppetsc/Mesh.h"
#include "ae108/cpppetsc/ParallelComputePolicy.h"
#include "ae108/cpppetsc/Vector.h"
#include "ae108/cpppetsc/Viewer.h"
#include "ae108/cpppetsc/asThAT.h"
#include "ae108/cpppetsc/asTransposedMatrix.h"
#include "ae108/cpppetsc/computeElementsOfMatrix.h"
#include "ae108/cpppetsc/createVectorFromSource.h"
#include "ae108/cpppetsc/setName.h"
#include "ae108/cpppetsc/writeToViewer.h"
#include "ae108/cppslepc/Context.h"
#include "ae108/elements/TimoshenkoBeamElementWithMass.h"
#include "ae108/elements/mesh/read_mesh_from_file.h"
#include "ae108/elements/mesh/refine_segment_mesh.h"
#include "ae108/elements/tensor/as_vector.h"
#include "ae108/solve/boundaryConditionsToTransform.h"

#include <fstream>
#include <iostream>
#include <sstream>

using namespace ae108;

using Policy = cpppetsc::ParallelComputePolicy;
using Context = cppslepc::Context<Policy>;
using Mesh = cpppetsc::Mesh<Policy>;
using Vector = cpppetsc::Vector<Policy>;
using Viewer = cpppetsc::Viewer<Policy>;
using BoundaryCondition = cpppetsc::GeneralizedMeshBoundaryCondition<Mesh>;

constexpr auto topological_dimension = Mesh::TopologicalDimension{1};
constexpr auto coordinate_dimension = Mesh::CoordinateDimension{3};
const auto number_of_eigenvalues = Mesh::size_type(18);

using Point = std::array<Mesh::real_type, coordinate_dimension>;

using Element = elements::TimoshenkoBeamElementWithMass<
    coordinate_dimension, Vector::value_type, Vector::real_type>;

using Properties =
    elements::TimoshenkoBeamWithMassProperties<Mesh::real_type,
                                               coordinate_dimension>;

using Plugins =
    assembly::FeaturePlugins<assembly::plugins::AssembleEnergyPlugin,
                             assembly::plugins::AssembleForceVectorPlugin,
                             assembly::plugins::AssembleStiffnessMatrixPlugin,
                             assembly::plugins::AssembleMassMatrixPlugin>;

using Assembler = assembly::Assembler<Element, Plugins, Policy>;

using elements::tensor::as_vector;

int main(int argc, char **argv) {
  const auto context = Context(&argc, &argv);

  // Introduce the example.
  PetscPrintf(Policy::communicator(), "%s", std::string(90, '#').c_str());
  PetscPrintf(Policy::communicator(),
              "\nCompare to Choi et al. (2019), Figure 6(b)"
              "\nhttps://doi.org/10.1038/s41598-019-46089-9"
              "\n");
  PetscPrintf(Policy::communicator(), "%s", std::string(90, '#').c_str());
  PetscPrintf(Policy::communicator(), "\n");

  std::string data_folder = "/mnt/io/validation/data/";
  std::string output_folder = "";

  std::ifstream fConfig(data_folder + "cube.config");
  const auto config = read_config<coordinate_dimension>(fConfig, 1);

  std::ifstream fKpath(data_folder + config.name + ".kpath");
  FILE *out = fopen((output_folder + config.name + ".dispersion").c_str(), "w");

  auto properties = circular_beam_properties<Properties>(
      config.young_modulus, config.poisson_ratio, config.radius,
      config.density);

  const auto &lattice_vectors = config.lattice_vectors;

  const auto geometry = elements::mesh::refine_segment_mesh(
      elements::mesh::read_mesh_from_file<Point>(data_folder + config.name +
                                                 ".dat"),
      0.04 / 10);

  const auto mesh = Mesh::fromConnectivity(
      topological_dimension, coordinate_dimension, geometry.connectivity(),
      geometry.number_of_positions(), Element::degrees_of_freedom(), 0);

  auto assembler = Assembler();

  auto element_source = ae108::homogenization::utilities::assign_lattice_to(
      geometry.connectivity(), geometry.positions(), lattice_vectors);

  for (const auto &element : mesh.localElements()) {
    elements::tensor::Tensor<Mesh::real_type, coordinate_dimension>
        element_axis;
    as_vector(&element_axis) =
        as_vector(&geometry.position_of_vertex(
            geometry.connectivity().at(element.index()).at(1))) -
        as_vector(&geometry.position_of_vertex(
            geometry.connectivity().at(element.index()).at(0)));

    const auto weight =
        1. / std::count(element_source.begin(), element_source.end(),
                        element_source[element.index()]);

    assembler.emplaceElement(
        element,
        Element::Element(weight * timoshenko_beam_stiffness_matrix(element_axis,
                                                                   properties)),
        weight *
            timoshenko_beam_consistent_mass_matrix(element_axis, properties));
  }

  const auto K = [&]() {
    auto K = Mesh::matrix_type::fromMesh(mesh);
    assembler.assembleStiffnessMatrix(Mesh::vector_type::fromLocalMesh(mesh),
                                      Element::Time{0.}, &K);
    K.finalize();
    return K;
  }();

  const auto M = [&]() {
    auto M = Mesh::matrix_type::fromMesh(mesh);
    assembler.assembleMassMatrix(&M);
    M.finalize();
    return M;
  }();

  const auto source_of = ae108::homogenization::utilities::assign_lattice_to(
      geometry.positions(), lattice_vectors);

  std::string line;
  while (std::getline(fKpath, line)) {
    std::stringstream linestream(line);
    std::string value;
    Point wave_vector({0, 0, 0});
    std::size_t i = 0;
    for (std::size_t i = 0; i < coordinate_dimension; i++) {
      std::getline(linestream, value, ',');
      wave_vector[i] = std::atof(value.c_str());
    }
    const auto T = [&](const Point &wave_vector) {
      std::vector<BoundaryCondition> boundary_conditions;
      for (const auto &vertex : mesh.localVertices()) {
        const Mesh::size_type &target = vertex.index();
        const Mesh::size_type &source = source_of[target];
        if (source != target) {
          const auto k = [&] {
            Point translation;
            as_vector(&translation) =
                as_vector(&geometry.position_of_vertex(target)) -
                as_vector(&geometry.position_of_vertex(source));
            return PETSC_i *
                   as_vector(&wave_vector).dot(as_vector(&translation));
          }();
          for (Mesh::size_type dof = 0; dof < vertex.numberOfDofs(); dof++) {
            boundary_conditions.push_back(
                {{target, dof}, {{std::exp(k), {source, dof}}}, 0.});
          }
        }
      }
      return solve::boundaryConditionsToTransform(boundary_conditions, mesh)
          .matrix;
    }(wave_vector);

    const auto KT = cpppetsc::asThAT(&K, &T);
    const auto MT = cpppetsc::asThAT(&M, &T);

    const auto result = cppslepc::computeGeneralizedEigenvaluesWithSINVERT(
        cpppetsc::computeElementsOfMatrix(KT),
        cpppetsc::computeElementsOfMatrix(MT), number_of_eigenvalues);

    PetscFPrintf(Policy::communicator(), out, "%f,%f,%f", wave_vector[0],
                 wave_vector[1], wave_vector[2]);
    for (std::size_t n = 0; n < number_of_eigenvalues; n++)
      PetscFPrintf(Policy::communicator(), out, ",%f",
                   std::sqrt(result[n]).real() / (2 * M_PI));
    PetscFPrintf(Policy::communicator(), out, "\n");
  }
  fKpath.close();
  fclose(out);
}