// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

template <class Properties>
const auto circular_beam_properties =
    [](typename Properties::real_type young_modulus,
       typename Properties::real_type poisson_ratio,
       typename Properties::real_type radius,
       typename Properties::real_type density) {
      using real_type = typename Properties::real_type;

      const real_type shear_modulus = young_modulus / (2 * (1 + poisson_ratio));
      const real_type shear_correction_factor =
          (7 + 6 * poisson_ratio) / 6 / (1 + poisson_ratio); // Cowper (1966)
      const real_type area = radius * radius * M_PI;
      const real_type area_moment = M_PI_4 * std::pow(radius, 4);
      const real_type polar_moment = M_PI_2 * std::pow(radius, 4);

      return Properties{{young_modulus, shear_modulus, shear_correction_factor,
                         shear_correction_factor, area, area_moment,
                         area_moment, polar_moment},
                        density};
    };
