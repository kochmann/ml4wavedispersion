// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ae108/cppslepc/computeEigenvalues.h"

namespace ae108 {
namespace cppslepc {

template <class Policy>
std::vector<
    std::complex<typename LinearEigenvalueProblemSolver<Policy>::real_type>>
computeGeneralizedEigenvaluesWithSINVERT(
    const cpppetsc::Matrix<Policy> &A, const cpppetsc::Matrix<Policy> &B,
    const std::size_t number_of_eigenvalues) {
  using value_type = typename LinearEigenvalueProblemSolver<Policy>::value_type;
  auto solver = LinearEigenvalueProblemSolver<Policy>{};
  ST st;

  Policy::handleError(EPSGetST(solver.data(), &st));
  Policy::handleError(STSetType(st, STSINVERT));
  Policy::handleError(
      EPSSetWhichEigenpairs(solver.data(), EPS_TARGET_MAGNITUDE));
  Policy::handleError(EPSSetTarget(solver.data(), value_type(0, 0)));
  Policy::handleError(EPSSetDimensions(solver.data(), number_of_eigenvalues,
                                       PETSC_DECIDE, PETSC_DECIDE));
  Policy::handleError(EPSSetOperators(solver.data(), A.data(), B.data()));
  return detail::solve(solver);
}

} // namespace cppslepc
} // namespace ae108