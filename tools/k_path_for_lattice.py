def k_path_for(lattice):

    from pymatgen.core.structure import Structure
    from pymatgen.symmetry.bandstructure import HighSymmKpath
    from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
    import numpy as np

    s = Structure(np.nan_to_num(lattice), [1], [[0, 0, 0]])
    sa = SpacegroupAnalyzer(s)
    ps = sa.get_primitive_standard_structure(international_monoclinic=False)
    h = HighSymmKpath(ps, symprec=0.001, angle_tolerance=1, atol=1e-06)

    return h.get_kpoints(line_density=20)