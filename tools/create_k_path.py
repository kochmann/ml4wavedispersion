import sys
import numpy as np
from k_path_for_lattice import k_path_for

output = sys.argv[2]
first = int(sys.argv[3])
last = int(sys.argv[4])

input = np.loadtxt(
    sys.argv[1],
    skiprows=1 + first,
    max_rows=last,
    delimiter=",",
    usecols=(4, 5, 6, 7, 8, 9, 10, 11, 12),
    ndmin=2,
)

names = np.loadtxt(
    sys.argv[1],
    skiprows=1 + first,
    max_rows=last,
    delimiter=",",
    usecols=(13),
    dtype=str,
    ndmin=2,
)

for i in range(last - first):

    kpath = k_path_for(np.reshape(input[i, :], (3, 3)))
    f = open(output + names[i][0] + ".kpath", "w")
    for label, k in zip(kpath[1], kpath[0]):
        f.write(str(k[0]) + "," + str(k[1]) + "," + str(k[2]))
        if label:
            f.write("," + label)
        f.write("\n")
    f.close()
