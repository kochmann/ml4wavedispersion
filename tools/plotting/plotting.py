import numpy as np
from matplotlib import pyplot as plt
import sys

for i in range(100):
    try:
        name = "UC_" + str(i)
        print(name)

        # name = sys.argv[1]

        # name = "UC_1"
        path = "/mnt/io/structures/"
        # path = "/home/bastian/Workspace/cppPNCOpt/build/Applications/"
        fkpath = path + name + ".kpath"
        fdpath = path + name + ".dispersion"

        plt.rc("text", usetex=True)
        plt.rc("font", family="serif")

        dispersion = np.loadtxt(fdpath, delimiter=",")

        with open(fkpath, "r") as f:
            kpath = [x.split(",") for x in f.readlines()]

        klabel = []
        il = []
        for i, k in enumerate(kpath):
            if len(k) > 3:
                klabel.append(k[3].rstrip())
                il.append(i)

        np.set_printoptions(precision=8, edgeitems=50, linewidth=2)
        eigfreq = dispersion[:, 3:]
        kpath = dispersion[:, :3]

        # plt.rc('text', usetex=True)

        from matplotlib.patches import Rectangle

        currentAxis = plt.gca()
        i_klabel = []
        for i in range(0, len(il), 2):

            idb = il[i]
            idu = il[i + 1]

            plt.axvline(x=idb - i, color="black", linestyle="--", linewidth=0.8)
            plt.plot(
                range(idb - i, idu - i),
                eigfreq[range(idb, idu)],
                color="black",
                marker=",",
                linestyle="None",
            )
            # plt.gca().set_prop_cycle(None)

            i_klabel.append(idb - i)

            fu = np.nanmin(eigfreq[idb:idu], axis=0)[1:]
            fl = np.nanmax(eigfreq[idb:idu], axis=0)[:-1]

            pb = fu - fl

            for j, b in enumerate(pb):
                if b > 2e-2:
                    w = idu - idb - 1
                    h = fu[j] - fl[j]
                    # print(h)

                    currentAxis.add_patch(
                        Rectangle((idb - i, fl[j]), w, h, alpha=1, color="#98FB98")
                    )

        i_klabel.append(idu - i - 1)
        fu = np.nanmin(eigfreq, axis=0)[1:]
        fl = np.nanmax(eigfreq, axis=0)[:-1]
        full_bandgap = fu - fl
        for j, b in enumerate(full_bandgap):
            if b > 2e-2:

                w = i_klabel[-1]
                h = fu[j] - fl[j]

                print(h)

                currentAxis.add_patch(
                    Rectangle((0, fl[j]), w, h, alpha=1, color="#6B8E23")
                )

        # # k-path labelling
        klabel2 = []
        double = False
        for i in range(len(klabel) - 1):

            idb = i
            idu = i + 1
            if il[idb] == il[idu] - 1:
                if klabel[idb] != klabel[idu]:
                    klabel2.append(
                        "$" + klabel[idb] + r"\vert " + klabel[idb + 1] + "$"
                    )
                    double = True
            else:
                if not double:
                    klabel2.append("$" + klabel[idb] + "$")
                else:
                    double = False
        klabel2.append("$" + klabel[idu] + "$")

        plt.xticks(i_klabel, klabel2)
        plt.xlim(0, len(kpath) - len(klabel))
        plt.ylim(bottom=0)
        # plt.title('dispersion curve')
        plt.xlabel("k-space position")
        plt.ylabel("frequency $[Hz]$")
        plt.savefig(name + ".png")
        plt.close()
    except:
        pass