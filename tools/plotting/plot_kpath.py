from scipy.spatial import Voronoi
from itertools import repeat
import numpy as np


def cartesian(arrays, out=None):

    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = int(n / arrays[0].size)
    out[:, 0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m, 1:])
        for j in range(1, arrays[0].size):
            out[j * m : (j + 1) * m, 1:] = out[0:m, 1:]
    return out


# lattice_vectors = np.array(
#     [
#         [-0.636522924, 0.97629751, -0.040878218],
#         [-0.42380483, -0.35052564, 0.680946673],
#         [0.905771127, -0.491217243, 0.956755703],
#     ]
# )
# lattice_vectors = np.transpose(lattice_vectors)
lattice_vectors = np.array(
    [
        [1.5, 0, 0],
        [0, 1, 0],
        [0, 0, 1],
    ]
)

from pymatgen.core.structure import Structure
from pymatgen.symmetry.bandstructure import HighSymmKpath

fac = 1

s = Structure(np.nan_to_num(lattice_vectors) / fac, [1], [[0, 0, 0]])
h = HighSymmKpath(s, symprec=0.001, angle_tolerance=1, atol=1e-06)

kpath = h.get_kpoints(line_density=1)
kpath = ([k / fac for k in kpath[0]], kpath[1])  # back scale

k = cartesian(list(repeat([-2, -1, 0, 1, 2], 3)))

r = np.dot(k, h.prim_rec.matrix / fac)

vor = Voronoi(r)

# Find unit cell vertices
v_idx = np.argwhere(
    [np.all(np.greater_equal(reg, 0)) for reg in vor.regions]
)  # regions with all vertices in a unit cell

# Find unit cell at origin
pidx = np.where(np.sum(vor.points == [0.0, 0.0, 0.0], axis=1) == 3)[0][0]
ridx = vor.point_region[pidx]
region = vor.regions[ridx]

# Find unit cell edges from set of all found Voronoi ridges
edges = []
for vridge in vor.ridge_vertices:

    # if vridge is part of region it is part of the unit cell
    if np.all(np.in1d(vridge, region)):

        # Remap edge coordinates from Voronoi tesselation to unit cell
        edge = []
        for vortex in vridge:

            edge.append(np.argwhere(np.asarray(region) == vortex)[0][0])

        edges.append(np.asarray(edge))

vertices = vor.vertices[region]
edges = np.asarray(edges)

import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()
plt.rc("text", usetex=True)
plt.rc("font", family="serif")

ax = fig.gca(projection="3d")
for edge in edges:

    edge = np.append(edge, edge[0])
    v = vertices[edge]
    ax.plot(v[:, 0], v[:, 1], v[:, 2], "black")


kx = [k[0] for k in kpath[0]]
ky = [k[1] for k in kpath[0]]
kz = [k[2] for k in kpath[0]]

klabel = np.array(kpath[1])
idx = np.where(klabel != u"")[0]

for i, j in zip(range(0, len(idx) - 1, 2), range(1, len(idx), 2)):
    x = [kx[idx[i]], kx[idx[j]]]
    y = [ky[idx[i]], ky[idx[j]]]
    z = [kz[idx[i]], kz[idx[j]]]
    # if np.sum(z)==0:
    ax.plot(x, y, z, color="b")
# if np.sum(z)==0:
ax.plot(x, y, z, label="kpath", color="b")

ax.plot([0, 0], [0, 0], [0, 1], color="r")

for i in idx:

    ax.text(kx[i], ky[i], kz[i], "$" + klabel[i] + "$", size=20, zorder=1, color="k")

ax.xaxis.set_visible(False)
ax.yaxis.set_visible(False)
ax.zaxis.set_visible(False)
ax.set_axis_off()
plt.savefig("kpath.svg")
# plt.show()